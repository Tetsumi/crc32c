#lang scribble/manual

@require[@for-label[crc32c
                    racket/base]
         scribble/examples]

@(define my-eval (make-base-eval))
@(my-eval '(require racket/base crc32c))

@title{crc32c}
@author+email["Tetsumi" "testumi@protonmail.com"]

@defmodule[crc32c]

This package provides an implementation of CRC32-C.

CRC32-C is specified as the CRC that uses the iSCSI polynomial (#x1edc6f41)
in @hyperlink["https://tools.ietf.org/html/rfc3720#section-12.1"]{RFC 3720}.
The polynomial was introduced by G. Castagnoli, S. Braeuer and M. Herrmann.

@section{Basic Generation}

@defproc[(crc32c-bytes [bytes bytes?]) fixnum?]{
  Generates a checksum from a @racket[bytes?] object.
	
  @examples[#:eval my-eval
    (crc32c-bytes #"Lorem ipsum dolor sit amet")
  ]
}

@defproc*[([(crc32c-string/utf8 [string string?]) fixnum?]
           [(crc32c-string/latin-1 [string string?]) fixnum?]
	   [(crc32c-string/locale [string string?]) fixnum?])]{
  Generates a checksum from a @racket[string?] object.
   @examples[#:eval my-eval
    (crc32c-string/utf8 "Lorem ipsum dolor sit amet")
  ]
}

@defproc[(crc32c-input-port [in input-port? (current-input-port)])
         fixnum?]{
  Generates a checksum reading bytes from an @racket[input-port?] object.
	
  @examples[#:eval my-eval
    (crc32c-input-port (open-input-bytes #"Lorem ipsum dolor sit amet"))

    (crc32c-input-port (open-input-string "Lorem ipsum dolor sit amet"))
  ]
}

@section{Incremental Generation}

The following procedures generate a checksum incrementally, one byte at a time.

@defthing[crc32c-initial-value fixnum? #:value #xFFFFFFFF]{
  Initial value to use to start the generation of a checksum.
}

@defproc[(crc32c-update [acc fixnum?] [byte byte?])
         fixnum?]{
  Returns updated @racket[acc] after using @racket[byte].

  @examples[#:eval my-eval
    (crc32c-update crc32c-initial-value 123)
  ]
}

@defproc[(crc32c-finalize [acc fixnum?]) fixnum?]{
  Finalizes the generation by applying the final XOR operation to @racket[acc].
  @examples[#:eval my-eval
    (crc32c-finalize (crc32c-update crc32c-initial-value 123))
    
    (for/fold ([acc crc32c-initial-value]
               #:result (crc32c-finalize acc))
	      ([ch "Lorem ipsum dolor sit amet"])
      (crc32c-update acc (char->integer ch)))
  ]
}
