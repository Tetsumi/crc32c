crc32c
======

An implementation of CRC32-C written in Racket.

![LGPLv3](https://www.gnu.org/graphics/lgplv3-147x51.png)
